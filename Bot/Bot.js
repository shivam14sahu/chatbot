import React, {useEffect} from 'react';
import {Animated, View,StyleSheet, Text, Easing} from 'react-native';
import {connect} from 'react-redux';


const Bot=(props)=>{
  const fadeValue = new Animated.Value(0);
  const animatedVal = new Animated.Value(-50);
  const {currentStep} = props;
  
  const animation = ()=>{
    Animated.parallel([
      Animated.timing(fadeValue, {
        toValue: 1,
        duration: 2000
      }),
      Animated.timing(animatedVal, {
        toValue: 1,
        duration: 300,
        easing: Easing.linear
      })
    ]).start();
  }
  useEffect(() => {
    animation();
  }, [currentStep.question])
  return (
    <View>
    <Animated.Text style={{
      ...styles.question, top:animatedVal, opacity:fadeValue
    }}>
      {currentStep.question ? <Text>{currentStep.question}</Text> : null}
    </Animated.Text>
    </View>
  );
}

const mapStateToProps=(state)=>{
  return state;
}

const styles = StyleSheet.create({
  question:{
    fontSize:30,
    paddingVertical:20,
    paddingLeft:10
  }
})

export default connect(mapStateToProps, null)(Bot);