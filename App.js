import React,{useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TouchableOpacity
} from 'react-native';

import Bot from '@Bot/Bot';
import User from '@User/User';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { fetchStep, onBack} from '@Redux/RootAction';


const App= (props) =>{
  const { historyLog, previousResponse } = props;
  useEffect(()=>{
    props.fetchStep(props.currentStepId);
  },[]);
  const onBackPress = async () => {
    if (historyLog.length) {
      await props.onBack(historyLog[historyLog.length - 1]);
      await props.fetchStep(props.currentStepId - 2)
    }
  }
  return (
    <ScrollView contentContainerStyle={styles.mainContainer}>
      <TouchableOpacity onPress={() => onBackPress()} style={{padding: 10, alignSelf:"flex-end"}}>
        <Text >
          {previousResponse ? previousResponse:null}
        </Text>
      </TouchableOpacity>
    <View style={styles.container}>
      <Bot/>
      <User/>
    </View>
    </ScrollView>
  );
};

const mapDispatchToProps = dispatch=>{
  return bindActionCreators({ fetchStep, onBack}, dispatch)

}

const mapStateToProps= (state)=>{
  return state;
}

const styles = StyleSheet.create({
  mainContainer:{
    height:'100%',
    justifyContent:'space-between',
    backgroundColor: '#DCDCDC', 
  },
  container:{
    paddingBottom:30,
    elevation:1,
    backgroundColor:'white',
    borderTopStartRadius:30,
    borderTopEndRadius:30
  }
})
export default connect(mapStateToProps,mapDispatchToProps)(App);
