import { FETCH_STEP, ON_BACK,SUBMIT_RESPONSE } from './types';
const initialState = {
  historyLog:[],
  currentStep:{},
  previousResponse:null,
  end:false,
  currentStepId:null
}

const RootReducer = (state=initialState, action)=>{
  const {payload, type} = action;
  switch(type){

    case(FETCH_STEP):{
      const currentStep = payload;
      const {id:currentStepId} = currentStep;
      return{...state, currentStep, currentStepId};
    };
    case(ON_BACK):{
      const {historyLog} = state;
      let previousResponse = null;
      historyLog.pop();
      if(historyLog.length){
        previousResponse = historyLog[historyLog.length-1].response;
      }
      return{...state, historyLog, previousResponse};
    }
    case(SUBMIT_RESPONSE):{
      const {historyLog} = state;
      historyLog.push(payload);
      const previousResponse=payload.response;
      return {...state, historyLog, previousResponse};
    };
    default:{
      return state;
    };
  }
}

export default RootReducer;