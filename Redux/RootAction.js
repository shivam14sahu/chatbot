import { FETCH_STEP, ON_BACK, SUBMIT_RESPONSE} from './types';
import axios from 'axios';

export const fetchStep =(currentStepId)=>async dispatch=>{
  const stepId = currentStepId ? currentStepId+1 : 1; 
  const response = await axios.get(`http://395b97df.ngrok.io/questions/${stepId}`) 
  .then(res=>res.data)
  .catch(error=>{throw error})
  dispatch({
    type:FETCH_STEP,
    payload:response
  })
}

export const onBack =(previousStep)=>async dispatch =>{
  const response = await axios.delete(`http://395b97df.ngrok.io/responses/${previousStep.id}`)
  .then(res=>res.data)
  .catch(error=>{throw error});
  dispatch({
    type: ON_BACK,
    payload:response
  })
}

export const submitResponse = (value, question)=>async dispatch=>{
  const response = await axios.post("http://395b97df.ngrok.io/responses",{question,response:value})
  .then(res=>res.data)
  .catch(error=>{throw error});
  dispatch({
    type:SUBMIT_RESPONSE,
    payload:response
  })
}

