import React, { useEffect } from "react";
import firebase from "react-native-firebase";
const App =()=>{
  useEffect(() => {

    firebase.messaging().subscribeToTopic('xyz');
    firebase.messaging().subscribeToTopic('qwe');
    firebase.messaging().subscribeToTopic('pqr');
    
    firebase.messaging().unsubscribeFromTopic('newTopic');


  }, []);  

  return (<Text>Hi There</Text>)
}

export default App;