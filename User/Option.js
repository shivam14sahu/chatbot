import React, {useEffect} from 'react';
import { Animated, StyleSheet, Text } from 'react-native';

const Option =({index, value})=>{
  const fadeValue = new Animated.Value(0);
  const animatedValue = new Animated.Value(200);

  const _animation = (index) => {
    Animated.sequence ([
      Animated.timing(fadeValue, {
        toValue: 1,
        duration:2000
      }),
      Animated.spring(animatedValue, {
        toValue: 0,
        duration: 1500,
        friction: 3,
        delay: index*300
      })
    ]).start();
  }
  useEffect(()=>{
    _animation(index);
  },[value])

  return(
      <Animated.View style={{ ...styles.option, opacity: fadeValue, transform: [{ translateX: animatedValue }] }} >
        <Text style={{ fontSize: 15, paddingVertical: 5, paddingHorizontal: 10 }}>{value}</Text>
      </Animated.View>
  )
}

const styles = StyleSheet.create({
  option: {
    backgroundColor: 'white',
    borderStyle: "solid",
    borderColor: '#A10DD9',
    borderWidth: 1,
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "flex-end"
  }
});

export default Option;