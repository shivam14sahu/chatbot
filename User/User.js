import React, { useState, useEffect } from "react";
import {
  Animated,
  Easing,
  Button,
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from "react-native";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { fetchStep, submitResponse,onBack } from "@Redux/RootAction";
import Option from './Option';

const User = (props) => {
  const [text, handleChangeText] = useState('');
  const { currentStep} = props;
  const {areOptions, end, isInput, options} = currentStep;

  const fadeValue =new  Animated.Value(0);
  const animatedValue =new Animated.Value(200);

  const _animation = () => {
    Animated.sequence ([
      Animated.timing(fadeValue, {
        toValue: 1,
        duration:2000
      }),
      Animated.spring(animatedValue, {
        toValue: 0,
        duration: 2000,
        friction: 3,
        delay: 100
      })
    ]).start();
  }

  useEffect(() => {
    _animation();
        
  }, [currentStep.question])


  const onSubmitResponse = async (value) => {
    await props.submitResponse(value, currentStep.question);
    !end ? await props.fetchStep(props.currentStepId) : null;
  }

  const renderOptions =() => {
    return (options.map(({ id, value },index) => (
      <TouchableOpacity key={id} onPress={() => onSubmitResponse(value)} 
      style={{padding:4,alignItems:'flex-end', marginRight:12}}>
      
      <Option value={value} index={index}/>
      </TouchableOpacity>
    )))
  }   
  
  return (
    <View style={{paddingTop:50}}>
      {
        areOptions ? 
        <Animated.View>
          {renderOptions()}
        </Animated.View>
          : isInput ? (
            <Animated.View style={{opacity:fadeValue, transform:[{translateY:animatedValue}]}}>
            <KeyboardAvoidingView 
            contentContainerStyle={{
              flexDirection:'row',
              justifyContent:'space-between'
            }} 
            behavior='position'>
              <TextInput
                style={styles.textInput}
                onChangeText={text => handleChangeText(text)}
                value={text}
              />
              <Button title="Go" color='green'  onPress={() => onSubmitResponse(text)}  />
            </KeyboardAvoidingView>
            </Animated.View>
          ) : null
      }
    </View>
  );
};

const mapDispatchToProps = dispatch=>{
  return bindActionCreators({ fetchStep, submitResponse, onBack }, dispatch);
}

const mapStateToProps= (state)=>{
  return state;
}

const styles = StyleSheet.create({
  textInput: { 
    height: 40, 
    borderColor: "#8550C7", 
    marginLeft: 10, 
    borderWidth: 1, 
    padding: 10, 
    width: 300, 
    borderRadius: 30 
  }
})
export default connect(mapStateToProps, mapDispatchToProps)(User);
